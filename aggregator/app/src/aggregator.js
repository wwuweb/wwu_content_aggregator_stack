import recv_json from './parser';

const fetch = require('isomorphic-fetch');
const btoa = require('btoa');
const axios = require('axios');
const util = require('util');
const fs = require('fs');

const sort_by = 'DESC';
const sort_order = 'created';

const milliHour = 3600000; // constant for defining hours in milliseconds.

const sleep = util.promisify(setTimeout);

async function main(){
    let config;
    let times;

    try{
        config = await loadConfig();
    } catch(e){
        console.log("Error: Failed to load config.json.");
        console.log(e);
        console.log("Exiting...");
        return;
    }

    try {
        if(await !fs.existsSync(config.timePath)){
            await fs.writeFileSync(config.timePath, JSON.stringify({times: []}));
        }
    } catch(e){
        console.log("Error: Failed to create times.json.");
        console.log(e);
        console.log("Exiting...");
        return;
    }

    let endpoint = config.backendURL;
    let auth = `${process.env.DRUPAL_ACCOUNT_NAME}:${process.env.DRUPAL_ACCOUNT_PASSWORD}`;
    let b64_auth = btoa(auth);
    let startTime = parseConfigTime(config.startTime);

    if(!(startTime == 0) && !startTime){
        console.log("Error: Failed to parse aggregation start time. Usage: ");
        console.log("HH:MM (AM | PM)")
        console.log("Exiting...");
        return;
    }

    let scheduledTime = 24 + startTime;
    let shceduledDate = new Date();
    shceduledDate.setHours(scheduledTime, 1, 0, 0);
    let waitTime = shceduledDate.getTime() - Date.now();

    console.log(`Aggregation scheduled for: ${shceduledDate}.`);

    await sleep(waitTime);

    let period  = config.period * milliHour || 24 * milliHour;

    while(true){
        try {
            await waitForBackend(endpoint);
        } catch(e){
            console.log("Error: An unexpected error occurred while waiting for the backend.");
            console.log(e);
            console.log("Exiting...");
            return;
        }

        try{
            times = await loadTimes(config.timePath);
        } catch(e){
            console.log("Error: Failed to load times.json.");
            console.log(e);
            console.log("Exiting...");
            return;
        }

        console.log("Backend connection successful. Begginning aggregation...");

        for(let i = 0; i < config.hosts.length; i++){
            let timeIndex = times.times.findIndex(item => item.name === config.hosts[i].name);
            var date = timeIndex != -1 ? times.times[timeIndex].date : 0;

            console.log(`Date: ${new Date(date)}`);
            console.log(`Aggregating from: ${config.hosts[i].name}`);

            recv_json(config.hosts[i].url, date, sort_by, sort_order, config.maxSourceTries).then(
                async result => {
                    console.log(`Fetched ${result.data.length} articles.`);
                    console.log("Aggregating...");

                    for(let i = 0; i < result.data.length; i ++){
                        const waitForAggregate = async (item) => {
                            try {
                                await aggregate(endpoint, item, b64_auth);
                            }catch(e){
                                console.log('Error: Failed to aggregate article: ');
                                console.log(e);
                            }
                        }

                        console.log(`Aggregating article: ${i + 1}`);
                        await sleep(150);
                        await waitForAggregate(result.data[i]);
                    }

                    date = result.latestDate;
                    console.log(`Success. Completed aggregation at: ${new Date(date)}`);

                    let timeObj = {
                        name: config.hosts[i].name,
                        date: date
                    };

                    if(timeIndex != -1){
                        times.times[timeIndex] = timeObj;
                    } else {
                        times.times.push(timeObj);
                    }

                    fs.writeFileSync(config.timePath, JSON.stringify(times));
                }
            );
        }

        console.log("Sleeping until next aggregation.");

        await sleep(period);
    }
}

async function loadConfig(){
    return await JSON.parse(fs.readFileSync('./config.json'));
}

async function loadTimes(path){
    return await JSON.parse(fs.readFileSync(path));
}

async function waitForBackend(endpoint){
    let success = false;
    let response;

    while(!success){
        let error;

        try {
            response = await fetch(endpoint);
        } catch(e){
            console.log("An error occurred while checking backend status: ");
            console.log(e);
            error = e;
        }

        if(error || response.status > 299){
            console.log("Waiting for backend...");
        } else {
            console.log("Backend is available.");
            success = true;
        }

        sleep(1000);
    }
}

async function aggregate(endpoint, item, b64_auth){
    let postData = {
        data: {
            type: "node--content",
            attributes: item
        }
    }

    let response;
    let error;

    try {
        response = await axios({
            method: 'POST',
            url: endpoint,
            data: JSON.stringify(postData),
            headers: {
                'Accept': 'application/vnd.api+json',
                'Content-Type': 'application/vnd.api+json',
                'Authorization': `Basic ${b64_auth}`
            }
        });
    } catch(e){
        console.log("Caught error in post step:");
        console.log(e);
        error = e;
    }

    if(error || response.status > 299){
        console.log("Caught error in response...");
        response ? console.log(response) : null;
    }

    return response;
}

function parseConfigTime(configTime){
    let fullRegex = /[0-9]{1,2}:[0-9]{2}( |\t)*([Aa][Mm]|[Pp][Mm])$/;

    if(!fullRegex.test(configTime)){
        return null;
    }

    let hour = parseInt(configTime.match(/[0-9]{1,2}/g)[0]);
    let amPm = configTime.match(/([Aa][Mm]|[Pp][Mm])/g)[0];

    if(/[Pp][Mm]/.test(amPm)){
        hour += 12;
    }

    if(hour > 13 || hour < 1){
        console.log("Must be an hour between 1-12");
        return null;
    }

    if(/[Aa][Mm]/.test(amPm) && hour == 12){
        hour = 0;
    }

    return hour;
}

main();
