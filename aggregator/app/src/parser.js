const fetch = require("isomorphic-fetch");

//recieves json aggregation compliant service
//get all articles until date by ascending or descending order in created or updated mode
//sort_order "DESC" for descending or "ASC" for ascending
//sort_by either "updated" or "created"

/**
 * This function recieves json from an aggreation compliant service the service should 
 * respond with an object of the following format:
 * 
 * {
 *  total_items: num,
 *  current_page: num,
 *  total_pages: num,
 *  result: [
 *  {
 *      field_url: str,
 *      field_created: str (Date),
 *      field_updated: str (Date),
 *      field_body: str,
 *      title: str,
 *  },
 *  ...
 *  ]
 * }
 * 
 * @param {*} url 
 * @param {*} date 
 * @param {*} sort_order 
 * @param {*} sort_by
 * @param {*} maxTries
 */
const recv_json = async (url, date, sort_order, sort_by, maxTries) =>{
    let articles = [];

    /**
     * Prints out an error based upon the result of a request
     * @param {*} err 
     * @param {*} res 
     */
    const handleRejection = (err, res) => {
        console.log("====AGGREGATION REQUEST ERROR=====");
        err ? console.log(err) : console.log(res);
    }

    /**
     * fetches articles from URL
     * @param {*} url 
     */
    const getArticles = async (url) => {
        let gotArticles = false;
        let attempt = 0;

        while(!gotArticles && attempt < maxTries){
            let res;
            let err = null;
            res = await fetch(url).catch(e => err = e);
            
            if(err || res.status >= 299){
                handleRejection(err, res);
            } else {
                gotArticles = true;
                data = await res.json().catch(e => handleRejection(e, res));
            }

            attempt ++;
        }

        return data;
    }

    let currDate = Number.POSITIVE_INFINITY;
    let numPages = Number.POSITIVE_INFINITY;
    let latestDate = 0;
    let currPage = 0;
    let data;

    while(date < currDate && currPage < numPages){

        console.log(`PAGE: ${currPage}/${numPages}`);
        let url_call = url.concat('sort_order='+sort_order, '&sort_by='+sort_by, '&page='+currPage);

        data = await getArticles(url_call);
        numPages = data.total_pages;

        data.result.forEach(function(item){

            currDate = new Date(sort_by === 'created' ? item.field_created : item.field_updated).getTime();
            if(sort_by === 'created'){
                latestDate = new Date(new Date(item.field_created).getTime() > latestDate ? item.field_created : latestDate).getTime();
            } else {
                latestDate = item.updated > latestDate ? item.updated : latestDate; //UNTESTED, dont use this please...
            }

            if(new Date(item.field_created).getTime() <= date){
                return;
            }

            let images = []
            var found = item.field_body.indexOf("src")
            while(found != -1){
                var urlEnd = item.field_body.indexOf(" ", found)
                var url = item.field_body.substring(found+4, urlEnd)
                images.push(url)
                found = item.field_body.indexOf("src", found+1)
            }
            item.field_images = images;
    
            //remove extraneous HTML junk from body...
            item.field_body = item.field_body.replace(/<\/?[^>]+>/gi, '');

            articles.push(item);
        });

        currPage ++;
    }

    return {
        data: articles,
        latestDate: latestDate
    };
}

export default recv_json;

