import Link from 'next/link';
import styles from '../styles/modules/Header.module.css';

const Header = () => (
  <div className={styles.header}>
    <h1 className={styles.headerText}>
      <Link href="/">
        <a className={styles.headerLink}>Western Content Aggregator</a>
      </Link>
    </h1>
  </div>
);

export default Header;
