import { Header } from '../components'

const Container = ({children}) => (
  <div className="container">
    <Header />
    {children}
  </div>
);

export default Container;
