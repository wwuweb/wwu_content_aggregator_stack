import styles from '../styles/modules/ContentLayout.module.css';

const ContentLayout = ({ children }) => <div className={styles.contentLayout}>{children}</div>

export default ContentLayout;
