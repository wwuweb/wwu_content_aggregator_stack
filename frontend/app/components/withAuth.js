import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { applySession } from 'next-session';
import { options } from '../utils/session';
import { Main } from '../components';

export function withAuthServerSideProps(getServerSideProps) {
  return async (ctx) => {
    await applySession(ctx.req, ctx.res, options);

    const { props } = getServerSideProps ? getServerSideProps(ctx) : {};

    return {
      props: {
        ...props,
        ...{ authorized: ctx.req.session.authorized || false }
      }
    }
  }
}

export default function withAuth(WrappedComponent) {
  return (props) => {
    const { authorized } = props;
    const router = useRouter();

    useEffect(() => {
      if (!authorized) {
        router.replace('/login');
      }
    });

    if (authorized) {
      return <WrappedComponent {...props} />
    }
    else {
      return <Main/>
    }
  }
}
