import Head from 'next/head';

const PageTitle = ({children}) => (
  <Head>
    <title>Western Content Aggregator | {children}</title>
  </Head>
);

export default PageTitle;
