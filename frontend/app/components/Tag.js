import Link from 'next/link';
import styles from '../styles/modules/Tag.module.css';

const Tag = ({ id, name }) => (

  /*
  handleHTTPErr(error, status) {
    console.log(`====REQUEST ERROR: ${status}=====`);
    error.errors.map(function(err){
      console.log(`title: ${err.title}`);
      console.log(`status: ${err.status}`);
      console.log(`detail: ${err.detail}`);
    });
    console.log('======================');
  }

  handleDelete = event => {
    this.delete().then(() => {
      console.log('Tag successfully deleted.');
    })
  }

  async delete() {
    const tagComponent = this;

    const { id, contentId } = this.props;

    const tagEndPoint = process.env.BACKEND_URL + `/api/taxonomy_term/tags/${id}`;
    const contentEndPoint = process.env.BACKEND_URL + `/api/node/content/${contentId}`;

    const recievedTagRes = await fetch(tagEndPoint);
    const recievedTag = await recievedTagRes.json();

    const contentRes = await fetch(contentEndPoint);
    const contentInfo = await contentRes.json();

    const newTagRelationArr = recievedTag.data.relationships.field_content.data.filter(value => {
      return tagComponent.props.contentId !== value.id;
    });

    const newContentRelationArr = contentInfo.data.relationships.field_tags.data.filter(value => {
      return tagComponent.props.id !== value.id;
    });

    // Remove tag from content.
    fetch(contentEndPoint, {
      method: 'PATCH',
      mode: 'cors',
      headers: {
        'accept': 'applicaiton/vnd.api+json',
        'content-type': 'application/vnd.api+json'
      },
      body: JSON.stringify({
        data : {
          type: 'node--content',
          id: contentId,
          relationships: {
            field_tags: {
              data: newContentRelationArr
            }
          }
        }
      })
    }).then(async response => {
      if (response.status > 299) {
        let responseBody = await response.json();

        tagComponent.handleHTTPErr(responseBody, response.status);
      }
      else {
        tagComponent.props.refreshContent(tagComponent.props.contentId);
      }
    });

    // Remove content from tag.
    fetch(tagEndPoint, {
      method: 'PATCH',
      mode: 'cors',
      headers: {
        'accept': 'applicaiton/vnd.api+json',
        'content-type' : 'application/vnd.api+json'
      },
      body: JSON.stringify({
        data : {
          type: 'taxonomy_term--tags',
          id: recievedTag.data.id,
          relationships : {
            field_content : {
              data : newTagRelationArr
            }
          }
        }
      })
    }).then(async response => {
      if (response.status > 299) {
        let responseBody = await response.json();

        tagComponent.handleHTTPErr(responseBody, response.status);
      }
    });
  }
  */

  <span className={styles.tag}>
    <Link href={ {pathname: '/index', query: {id: id}} }>
      <a>{name}</a>
    </Link>
  </span>
);

export default Tag;
