const Main = ({children}) => <main className="content">{children}</main>

export default Main;
