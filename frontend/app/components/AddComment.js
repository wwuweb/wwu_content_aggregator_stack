import { useState } from 'react';
import styles from '../styles/modules/AddComment.module.css';

const AddComment = (props) => {
  const [ text, setText ] = useState('');

  const handleChange = event => {
    setText(event.target.value);
  }

  const handleSubmit = event => {
    event.preventDefault();

    const contentEndPoint = process.env.BACKEND_URL + `/api/node/content/${props.id}`;
    const comments = props.commentList || [];

    comments.push(text);

    fetch(contentEndPoint, {
      method: 'PATCH',
      mode: 'cors',
      headers: {
        'accept': 'application/vnd.api+json',
        'content-type' : 'application/vnd.api+json'
      },
      body: JSON.stringify({
        data : {
          type: 'node--content',
          id: props.id,
          attributes: {
            field_comments: props.commentList
          }
        }
      })
    }).then(async response => {
      console.log(response);

      if (response.status > 299) {
        let body = await response.json().catch(error => {
          console.log('ERROR PARSING RESPONSE: ');
          console.log(error);
        });

        const error = body;
        const { status } = response;

        console.log(`REQUEST ERROR: ${status}`);
        error.errors.map(err => {
          console.log(`title: ${err.title}`);
          console.log(`status: ${err.status}`);
          console.log(`detail: ${err.detail}`);
        });
      }
      else {
        props.refreshContent(props.id);
      }
    });
  }

  return (
    <div className={styles.addCommentWrapper}>
      <h3 className={styles.addCommentTitle}>Add a Comment</h3>
      <form onSubmit={handleSubmit}>
        <textarea className={styles.commentField} name="commentText" placeholder="Your comment" rows="10" onchange={handleChange}>{text}</textarea>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default AddComment;
