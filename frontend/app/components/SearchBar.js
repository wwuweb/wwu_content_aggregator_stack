import { useState } from 'react';
import styles from '../styles/modules/SearchBar.module.css';

const SearchBar = (props) => {
  const [ changed, setChanged ] = useState(false);
  const [ value, setValue ] = useState(props.value || '');

  const handleChange = event => {
    setChanged(true);
    setValue(event.target.value);
  }

  const handleSubmit = event => {
    event.preventDefault();

    if (changed) {
      props.handleSearch(value);
    }
  }

  return (
    <div className={styles.searchWrapper}>
      <form className={styles.searchForm} onSubmit={handleSubmit}>
        <label>
          <input className={styles.searchInput} type="text" placeholder="Search" value={value} onChange={handleChange} />
        </label>
        <button className={styles.searchSubmit} type="submit">
          <span className={`material-icons ${styles.searchIcon}`} aria-hidden="true">search</span>
        </button>
      </form>
    </div>
  );
}

export default SearchBar;
