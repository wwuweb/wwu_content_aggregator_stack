require('es6-promise').polyfill();
require('isomorphic-fetch');

export default class AddTag extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      recievingTags: false,
      value: ''
    };

    this.standardView = this.standardView.bind(this);
    this.recieveTagView = this.recieveTagView.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleHTTPErr(error, status) {
    console.log(`====REQUEST ERROR: ${status}=====`);
    error.errors.map(err => {
      console.log(`title: ${err.title}`);
      console.log(`status: ${err.status}`);
      console.log(`detail: ${err.detail}`);
    });
    console.log("======================");
  }

  handleChange(event) {
    this.setState({
      value: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.modifyTagList(this.state.value).then(() => {
      console.log("processed new tag request...");
    });

    this.handleClick();
  }

  handleClick() {
    let recievingTagsState = this.state.recievingTags;

    this.setState({
      recievingTags: !recievingTagsState
    });
  }

  async modifyTagList(term) {
    let contentEndPoint  = process.env.BACKEND_URL + '/api/node/content/' + this.props.contentId;
    let tagEndPoint = process.env.BACKEND_URL + '/api/taxonomy_term/tags/';
    let newTag = true;
    let addTagComponent = this;
    let recievedTag = await this.searchTags(term);

    let contentInfoRes = await fetch(contentEndPoint, {
      method : 'GET',
      mode: 'cors'
    });

    let contentInfo = await contentInfoRes.json();

    if (recievedTag) {
      newTag = this.isNewTag(recievedTag);
      tagEndPoint = tagEndPoint += recievedTag.id;
    }

    if (!newTag) {
      return;
    }

    if (recievedTag && newTag) {
      recievedTag.relationships.field_content.data.push({
        type : "node--content",
        id: this.props.contentId
      });

      contentInfo.data.relationships.field_tags.data.push({
        type : "taxonomy_term--tags",
        id: recievedTag.id
      });

      let newTagRelationArr = recievedTag.relationships.field_content.data;
      let newContentRelationArr = contentInfo.data.relationships.field_tags.data;

      // Add tag to content.
      fetch(contentEndPoint, {
        method: 'PATCH',
        mode: 'cors',
        headers: {
          'accept': 'application/vnd.api+json',
          'content-type' : 'application/vnd.api+json'
        },
        body: JSON.stringify({
          data : {
            type: "node--content",
            id: this.props.contentId,
            relationships : {
              field_tags : {
                data : newContentRelationArr
              }
            }
          }
        })
      }).then(async function(response) {
        if (response.status > 299) {
          let responseBody = await response.json();

          addTagComponent.handleHTTPErr(responseBody, response.status);
        }
        else {
          addTagComponent.props.refreshContent(addTagComponent.props.contentId);
        }
      });

      // Add content to tag.
      fetch(tagEndPoint, {
        method: 'PATCH',
        mode: 'cors',
        headers: {
          'accept': 'application/vnd.api+json',
          'content-type' : 'application/vnd.api+json'
        },
        body: JSON.stringify({
          data : {
            type: "taxonomy_term--tags",
            id: recievedTag.id,
            relationships : {
              field_content : {
                data : newTagRelationArr
              }
            }
          }
        })
      }).then(async function(response){
        if(response.status > 299){
          let responseBody = await response.json();

          addTagComponent.handleHTTPErr(responseBody, response.status);
        }
      });

    }
    else if (!recievedTag) {
      // POST: make a new tag and add to content.
      fetch(tagEndPoint, {
        method: 'POST',
        mode: 'cors',
        headers: {
          'accept' : 'application/vnd.api+json',
          'content-type' : 'application/vnd.api+json'
        },
        body : JSON.stringify({
          data : {
            type: 'taxonomy_term--tags',
            attributes: {
              name: term
            },
            relationships: {
              field_content : {
                data: [{
                  type: 'node--content',
                  id: this.props.contentId
                }]
              }
            }
          }
        })
      }).then(async function (response) {
        let responseBody = await response.json();

        if (response.status > 299) {
          addTagComponent.handleHTTPErr(responseBody, response.status);
        }
        else {
          let tagID = responseBody.data.id;

          contentInfo.data.relationships.field_tags.data.push({
            type : "taxonomy_term--tags",
            id: tagID
          });

          let newTagList = contentInfo.data.relationships.field_tags.data;

          fetch(contentEndPoint, {
            method: 'PATCH',
            mode: 'cors',
            headers: {
              'accept' : 'application/vnd.api+json',
              'content-type' : 'application/vnd.api+json'
            },
            body : JSON.stringify({
              data: {
                type: 'node--content',
                id: addTagComponent.props.contentId,
                relationships: {
                  field_tags: {
                    data: newTagList
                  }
                }
              }
            })
          }).then(async function(response) {
            let responseBody = await response.json();

            if (response.status > 299) {
              addTagComponent.handleHTTPErr(responseBody, response.status);
            }
            else {
              addTagComponent.props.refreshContent(addTagComponent.props.contentId);
            }
          });
        }
      });
    }
  }

  isNewTag(tag) {
    let foundTag = this.props.currTagList.find(function(item) {
      return item.id === tag.id;
    });

    return foundTag;
  }

  async getFullTagList() {
    let endPoint  = process.env.BACKEND_URL + '/api/taxonomy_term/tags'

    let rawResponse = await fetch(endPoint, {
      method: 'GET',
      mode: 'cors'
    });

    let response = await rawResponse.json()

    return response.data;
  }

  async searchTags(term) {
    let tagList = await this.getFullTagList(term);

    return tagList.find(tag => {
      return tag.attributes.name === term;
    });
  }

  standardView() {
    return (
      <button onClick={this.handleClick} className="Button">Add Tag</button>
    );
  }

  recieveTagView() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <button type="submit">Submit</button>
      </form>
    );
  }

  render() {
    return this.state.recievingTags ? this.recieveTagView() : this.standardView();
  }

}
