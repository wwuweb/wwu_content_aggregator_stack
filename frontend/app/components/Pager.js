import Link from 'next/link';
import { useState, useEffect } from 'react';
import { params } from '../utils/url';
import styles from '../styles/modules/Pager.module.css';

const Pager = ({ count, limit, path, query }) => {
  const pages = Math.ceil(count / limit);
  const page = query.page || 0;

  const back = () => {
    if (page > 1) {
      return path + params({...query, ...{'page': Number(page) - 1}});
    }
    else {
      return path + params({'q': query.q});
    }
  }

  const forward = () => {
    return path + params({...query, ...{'page': Number(page) + 1}});
  }

  return (
    <ul className={styles.pagerWrapper}>
    {(page > 0) && (
      <li className={styles.pagerBack}>
        <Link href={back()} shallow>
          <a className={styles.pagerLink}>
            <span className="material-icons">arrow_left</span>Previous
          </a>
        </Link>
      </li>
    ) || <span className={styles.pagerPlaceholder} />}
    {(pages > 1 && page < pages) && (
      <li className={styles.pagerForward}>
        <Link href={forward()} shallow>
          <a className={styles.pagerLink}>
            Next<span className="material-icons">arrow_right</span>
          </a>
        </Link>
      </li>
    ) || <span className={styles.pagerPlaceholder} />}
    </ul>
  );
}

export default Pager;
