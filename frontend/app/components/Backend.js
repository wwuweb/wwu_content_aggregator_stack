import { createContext, useContext } from 'react';

const BackendContext = createContext(null);

export const useBackend = () => useContext(BackendContext);

export const Backend = (props) => (
  <BackendContext.Provider value={props.endpoints}>
    {props.children}
  </BackendContext.Provider>
);
