import { FormattedDate } from '../components';
import styles from '../styles/modules/Content.module.css';

const Content = ({ value }) => (
  <div className={styles.contentWrapper}>
    <h2 className={styles.contentTitle}>{value.title}</h2>
    {value.created && (
      <div className={styles.contentDate}>
        <span className={styles.contentLabel}>Published</span> <FormattedDate>{value.created}</FormattedDate>
      </div>
    )}
    {value.modified && (
      <div className={styles.contentDate}>
        <span className={styles.contentLabel}>Updated</span> <FormattedDate>{value.modified}</FormattedDate>
      </div>
    )}
    {value.body && (
      <div className={styles.contentBody}>{value.body}</div>
    )}
    {value.link && (
      <div className={styles.contentUrl}>
        <span className={styles.contentLabel}>Original Source</span> <a href={value.link}>{value.link}</a>
      </div>
    )}
  </div>
);

export default Content;
