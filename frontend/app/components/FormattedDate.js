const FormattedDate = ({ children }) => {
  const date = new Date(children);
  const display = Intl.DateTimeFormat('en-US', {
    weekday: 'long',
    month: 'long',
    day: 'numeric',
    year: 'numeric'
  });

  return <span>{display.format(date)}</span>;
}

export default FormattedDate;
