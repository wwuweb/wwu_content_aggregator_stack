const ImageList = ({ sources }) => {
  let key = 0;

  return (
    <div className="image-list">
      {sources.map(src => <img src={src} key={key++} height="90" width="90" />)}
    </div>
  );
}

export default ImageList;
