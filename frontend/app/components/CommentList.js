import styles from '../styles/modules/CommentList.module.css';

const CommentList = (props) => {
  const { comments } = props;

  let key = 0;

  return (
    <div className={styles.commentList}>
      <h3 className={styles.commentListTitle}>Comments</h3>
      {comments ? comments.map(item => <li key={key++}>{item.value}</li>) : <span>No comments.</span>}
    </div>
  );
}

export default CommentList;
