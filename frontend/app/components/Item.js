import Link from 'next/link';
import { TagList } from '../components';
import styles from '../styles/modules/Item.module.css';

const Item = ({ value }) => (
  <div className={styles.item}>
    <h2>
      <Link href="/content/[uuid]" as={`/content/${value.uuid}`}>
        <a className={styles.itemTitleLink}>{value.title}</a>
      </Link>
    </h2>
    <div className={styles.itemExcerpt}>
      <div dangerouslySetInnerHTML={{ __html: value.excerpt }} />
    </div>
  </div>
);

export default Item;
