import { Tag } from '../components';
import styles from '../styles/modules/TagList.module.css';

const TagList = ({ tags, contentId, refreshContent }) => {
  const buildStandardTag = tag => (
    <Tag key={tag.id} id={tag.id} name={tag.name} />
  );

  const buildContentTag = tag => (
    <Tag key={tag.id} id={tag.id} name={tag.name} contentId={contentId} refreshContent={refreshContent} />
  );

  const buildTag = (contentId === undefined) ? buildStandardTag : buildContentTag;

  return (
    <div className={styles.tagList}>
      <h3 className={styles.tagListTitle}>Tags:</h3>
      {tags && tags.map(buildTag)}
    </div>
  );
}

export default TagList;
