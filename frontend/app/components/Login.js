import { WesternLogo } from '../components'
import styles from '../styles/modules/Login.module.css';

const Login = ({ link }) => (
  <div className={styles.loginContainer}>
    <WesternLogo />
    <a className={styles.loginLink} href={link}>Universal Login</a>
  </div>
);

export default Login;
