import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useBackend, Pager, SearchBar, SearchResults } from '../components';
import { params } from '../utils/url';
import styles from '../styles/modules/Search.module.css';

const Search = (props) => {
  const [ text, setText ] = useState(props.text || '');
  const [ count, setCount ] = useState(0);
  const { search } = useBackend();
  const router = useRouter();

  const handleSearch = value => {
    let url = props.path;

    if (value) {
      url += params({'q': value});
    }

    router.push(url, undefined, { shallow: true });
  }

  const handleResponse = value => {
    setCount(value);
  }

  useEffect(() => {
    setText(router.query.q);
  }, [router.query.q]);

  const searchClasses = [
    styles.search
  ];

  if (props.landing || !router.query.q) {
    searchClasses.push(styles.searchLanding);
  }

  if (text) {
    return (
      <div className={searchClasses.join(' ')}>
        <SearchBar value={text} handleSearch={handleSearch} />
        <SearchResults query={router.query} action={search} handleResponse={handleResponse} />
        <Pager count={count} limit={10} path={router.pathname} query={router.query} />
      </div>
    );
  }
  else {
    return (
      <div className={searchClasses.join(' ')}>
        <SearchBar handleSearch={handleSearch} />
      </div>
    );
  }
}

export default Search;
