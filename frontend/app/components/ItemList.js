import { Item } from '../components';

const ItemList = ({ items }) => (
  <div className="item-list">
    {items.map(item => <Item key={item.uuid} value={item} />)}
  </div>
);

export default ItemList;
