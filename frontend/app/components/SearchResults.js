import { useState, useEffect } from 'react';
import { ItemList } from '../components';

const SearchResults = ({ query, action, handleResponse }) => {
  const [ loading, setLoading ] = useState(true);
  const [ items, setItems ] = useState([]);

  useEffect(() => {
    action(query).then(result => {
      setLoading(false);
      setItems(result.data);
      handleResponse(result.count);
    });
  }, [query]);

  if (loading) {
    return <div className="spinner">Loading...</div>
  }
  else {
    return <ItemList items={items} />
  }
}

export default SearchResults;
