export async function validate(ticket, service) {
  const request = process.env.CAS_URL + '/serviceValidate' + '?service=' + encodeURI(service) + '&ticket=' + ticket;

  let result = false;
  let response = await fetch(request);

  if (response.status > 299) {
    console.log('CAS returned with an error.');
  }
  else {
    const text = await response.text();
    result = text.includes('authenticationSuccess');
  }

  return result;
}
