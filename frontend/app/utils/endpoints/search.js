const testSearchData1 = [
  {
    "title": "Western continues water testing at buildings on main campus",
    "body": "<p>Environmental Health and Safety (EHS) at Western Washington University is in the midst of water sampling at buildings throughout the main campus in Bellingham.  Since 2008, Western EHS has taken additional steps to monitor the campus’ drinking water. While there is no federal law requiring universities to test their drinking water, Western determined it in the best interest of the campus community to test priority sites on campus.  Western’s Drinking Water Management Program covers all campus buildings with many locations already having been sampled.",
    "excerpt": "… Washington University is in the midst of water sampling at <strong>building</strong>s throughout the main campus in Bellingham. Since … Drinking Water Management Program covers all campus <strong>building</strong>s with many locations already having been sampled. …",
    "uuid": "3881cb99-9c83-4ab3-90fa-52f7cae5fce9"
  },
  {
    "title": "Western continues water testing program at buildings on main campus",
    "body": "<p>Environmental Health and Safety (EHS) at Western Washington University is in the midst of its water sampling program at buildings throughout the main campus in Bellingham.  Since 2008, Western EHS has taken additional steps to monitor the campus’ drinking water. While there is no federal law requiring universities to test their drinking water, Western determined it in the best interest of the campus community to test priority sites on campus.  Western’s Drinking Water Management Program covers all campus buildings with many locations already having been sampled.",
    "excerpt": "… University is in the midst of its water sampling program at <strong>building</strong>s throughout the main campus in Bellingham. Since … Drinking Water Management Program covers all campus <strong>building</strong>s with many locations already having been sampled. …",
    "uuid": "792693b5-b308-4206-9c25-6bbbab0df664"
  },
  {
    "title": "WWU wins NASA contract to build new instrument to help Mars rover scientists",
    "body": "<p>A group of faculty and staff from Western Washington University has teamed with Seattle-based aerospace manufacturing company First Mode to win a new $302,000 contract from NASA to design and build a new instrument called a goniometer that will help scientists and members of the Mars rover teams better understand what they are seeing and sampling on the Red Planet.",
    "excerpt": "… Mode to win a new $302,000 contract from NASA to design and <strong>build</strong> a new instrument called a goniometer that will help … the data sent back from the rovers. The plans for <strong>building</strong> it and the software that runs it will then be made …",
    "uuid": "20e6d7fe-9d36-4e67-86bd-685a291ea6f7"
  },
  {
    "title": "Western Launches ‘Building Washington’s Future’ campaign; Project to Expand Western’s Capacity in Critical STEM Areas",
    "body": "<p>Each year, the state of Washington needs thousands more people prepared for jobs in science, technology, engineering and math (STEM) than our state’s universities can produce. Western Washington University is addressing this need by expanding specialized programs in key STEM fields.   Students in Western’s computer science, electrical engineering, and energy science and technology programs are being prepared to tackle the most pressing technology challenges of our time.",
    "excerpt": "… to improve cyber security, engineer smart grids and <strong>building</strong>s, create the energy systems of the future, and …",
    "uuid": "ada476b6-9700-4406-8df9-3cfb8b5dcef3"
  },
  {
    "title": "University to hold disaster-preparedness training exercise on Saturday, Oct. 26 at 32nd Street Building",
    "body": "<p>Western Washington University will conduct a training exercise&amp;nbsp;regarding operational readiness in the event of an earthquake on campus from 8 a.m. to noon on Saturday, Oct. 26,&amp;nbsp;at the University’s Administrative Services Building at 333 32nd Street, in Bellingham.&amp;nbsp; The training exercise will involve members of Western’s Emergency Management, Public Safety, Environmental Health and Safety, and University Communications.",
    "excerpt": "… Oct. 26, at the University’s Administrative Services <strong>Building</strong> at 333 32nd Street, in Bellingham.  The training … training exercise on Saturday, Oct. 26 at 32nd Street <strong>Building</strong> …",
    "uuid": "cea9ab98-4eca-479a-bf97-6f165dfc20e9"
  },
  {
    "title": "Board of Trustees Recap for Oct. 10 and Oct. 11",
    "body": "<p>Editor’s Note: After each Board of Trustees meeting, Western Today provides a recap of decisions and discussion.   &amp;nbsp;  Trustees Hear Western-Port Presentation on Waterfront Innovation Park  Western’s Board of Trustees on Friday heard a presentation on a public-private partnership model at the Bellingham waterfront.",
    "excerpt": "… and cybersecurity; • Companies focused on designing, <strong>building</strong>, operating and maintaining high-performance, energy-efficient <strong>building</strong>s.             Gibbs was joined in the presentation …",
    "uuid": "262f892d-b43b-46c8-99ea-9433d81a14f0"
  },
  {
    "title": "Western Alert Test scheduled for Thursday, Oct. 17; will include lockdown drill",
    "body": "<p>Dear Campus Community,  Western Washington University plans to test its Western Alert emergency communications system at 2:40 p.m. on Thursday, Oct. 17, and also conduct a campus lockdown drill.  The Western Alert&amp;nbsp;system&amp;nbsp;is a group of ways to reach students, faculty and staff with important safety information.  Lockdown Drill:&amp;nbsp;In conjunction with the test of the Western Alert system, a voluntary all-campus lockdown drill will take place at the same time – 2:40 p.m. on Thursday, Oct. 17.",
    "excerpt": "… Test voice messages over the campus fire alarm system (<strong>building</strong> enunciation). Sending text messages to everyone who … (<strong>building</strong> enunciation) will be repeated several times in <strong>building</strong>s as well as some outside speakers at: the …",
    "uuid": "a9e70f22-f44a-498c-bfa6-2ca2493ed8bd"
  },
  {
    "title": "Western&amp;#039;s Community Connect program to host dialogue on food systems, land use, and food access Nov. 5",
    "body": "<p>Western&#039;s Community Connect program will host &quot;Building Community Through&amp;nbsp;Food&amp;nbsp;Access and Education in Bellingham&quot; from noon to 1 p.m. on&amp;nbsp;Tuesday, Nov. 5, at the&amp;nbsp;WWU Outback Farm.",
    "excerpt": "… Western&#039;s Community Connect program will host &quot;<strong>Building</strong> Community Through Food Access and Education in … community? Join the discussion and be part of a plan to <strong>build</strong> our network of food advocates!   The …",
    "uuid": "c653bdf4-366a-4683-9ef3-309a99efb9f5"
  },
  {
    "title": "November Art Walk to feature WWU Design Projection Night",
    "body": "<p>Join the WWU Department of Design for November Art Walk on Nov. 1! The department,&amp;nbsp;with the support of Faithlife, will present a large-scale musical animation exhibition projected onto the side of the historic 7-story Flatiron building downtown Bellingham on Bay St.  The evening features 11 short animated pieces from BFA Design students playing on loop between 7-9 p.m.</p>\n",
    "excerpt": "… projected onto the side of the historic 7-story Flatiron <strong>building</strong> downtown Bellingham on Bay St. The evening features …",
    "uuid": "2740515d-805e-4867-baee-93686806301f"
  },
  {
    "title": "Learning about Place Through Food Dec. 10",
    "body": "<p>Please join us for Learning about Place Through Food on Tuesday, Dec.&amp;nbsp;10 from 5:30-7:30 pm at the Deming Public Library. \t \tCommunity Engagement Fellows is co-hosting this event alongside Common Threads, East Whatcom Regional Resource Center-Opportunity Council, PeaceHealth, Whatcom County Library System, WWU Community Health Program, WWU Salish Sea Institute, WWU Sustainable Communities Partnership, and York Community Farm. \t \tLearning about Place Through Food is open to all seeking to build and improve place-based education that focuses on local foods and food systems.",
    "excerpt": "… Learning about Place Through Food is open to all seeking to <strong>build</strong> and improve place-based education that focuses on … Fellows model. This approach generates relationship-<strong>building</strong> and knowledge-acquisition about each other&#039;s work, …",
    "uuid": "4a2bf5f4-8a65-4519-a676-05bdd9503b9f"
  }
];

const testSearchData2 = [
  {
    "title": "MBA Program info session set for Oct. 23",
    "body": "<p>The Western Washington University (WWU) Masters of Business Administration Program will be hosting its Fall Information Session on Wednesday, Oct. 23&amp;nbsp;at 6 p.m. in Parks Hall 441. This quarterly event is an opportunity to hear from an alumni panel, talk to faculty, and discuss with admissions advisors the details of the MBA Programs available at WWU.",
    "excerpt": "… tracks offer an enterprise management focus designed to <strong>build</strong> skills and establish managerial perspectives for … review, interviewing practice, and career related skill <strong>building</strong> On Wednesday, Oct. 23, the MBA welcomes students …",
    "uuid": "bcb44345-d164-4973-a295-98d2e1b3725b"
  },
  {
    "title": "WWU&amp;#039;s Small Business Development Center to Host a Rising Wages Workshop Nov. 20",
    "body": "<p>Western Washington&amp;nbsp;University’s Small Business Development Center (SBDC)&amp;nbsp;will host&amp;nbsp;a workshop designed to help local businesses respond to the increase in the minimum wage that goes into effect on Jan.&amp;nbsp;1. \t&amp;nbsp; \tIs your business prepared for a 12.5 percent increase in minimum wage? How will the coming change affect your profitability, employees, and customers? Join the SBDC Wednesday, Nov. 20 from 2:30 to 4:30 p.m.",
    "excerpt": "… Nov. 20 from 2:30 to 4:30 p.m. at the Dorothy Haggen <strong>Building</strong> in Barkley Village where a certified business … Nov. 20  from 2:30 to 4:30 p.m. Where: Dorothy Haggen <strong>Building</strong> Conference Room, 2211 Rimland Dr, Bellingham WA …",
    "uuid": "ba2c7f82-4e5a-4b9e-80d1-fae994f4f426"
  },
  {
    "title": "Sunday&amp;#039;s &amp;#039;Trails to Taps&amp;#039; race to weave through campus",
    "body": "<p>Sunday&#039;s &#039;Trails to Taps&quot; relay race starts at 9 a.m., and weaves through more than 30 miles of local trails, parks, and roadways, including the WWU campus. The race&#039;s 10th and final leg enters campus near College Hall, heads up to Haskell Plaza between Arntzen Hall and the Biology and Chemistry buildings, then circles back towards town by exiting campus via the Old Main Lawn and then Higginson. The race will end behind Boundary Bay Brewery on Railroad Avenue. Many runners may be in costume. Cheer them on if you get the chance!</p>\n",
    "excerpt": "… Plaza between Arntzen Hall and the Biology and Chemistry <strong>building</strong>s, then circles back towards town by exiting campus …",
    "uuid": "0d103f8e-6b19-4e7a-8d36-3126ab08ad04"
  },
  {
    "title": "Emily Borda Hired as New Director of SMATE at Western",
    "body": "<p>Emily Borda has been hired as director for the Science, Mathematics and Technology Education (SMATE) program at Western Washington University.  Borda,&amp;nbsp;a professor of Chemistry at Western, was hired following a national search and began her new duties on Sept. 16. She succeeds Ed Geary, who retired as SMATE director after working in the position&amp;nbsp;since 2013. &amp;nbsp;  “I am looking forward to working with Emily, who will continue Western’s well-known excellence in science education.",
    "excerpt": "… Her research is broadly aimed at investigating how students <strong>build</strong> mental models of chemical phenomena. She is interested … classroom and laboratory activities that help students <strong>build</strong> more coherent, useful models of chemical ideas. Most …",
    "uuid": "19085348-b05f-42e4-a590-cf2ff43198c4"
  },
  {
    "title": "WWU&amp;#039;s Men&amp;#039;s Resiliency Program holds cookout to launch new initiative",
    "body": "<p>WWU&#039;s Men’s Resiliency Program launched a new Male Success Initiative (MSI) with a Sunday Cookout on Oct. 6 at Forest and Cedar Park.&amp;nbsp; The event hosted 30+ people from Western Washington University, Whatcom Community College, and Northwest Indian College for a barbecue style cookout and introductory activities.",
    "excerpt": "… opportunities for the exploration of masculinity, the <strong>building</strong> of cross-cultural connection, and community service …",
    "uuid": "cdabc5a6-d290-494a-b18a-0b421f48ac70"
  },
  {
    "title": "Alan Gratz to join panel speaking about supporting refugees on Oct. 30 at WWU",
    "body": "<p>New York Times Bestselling Young Adult author Alan Gratz will join a panel exploring the question “What does it mean to support refugees?” at Western Washington University from 4-5 p.m. on Wednesday, Oct. 30&amp;nbsp;at the Western Gallery in the Fine Arts Building.   This collaborative dialogue event, co-sponsored by the Teaching-Learning Academy and the Western Gallery, will include an observed fishbowl dialogue among the speakers, followed by small group dialogue among participants.   The event is free and open to the public.",
    "excerpt": "… Wednesday, Oct. 30 at the Western Gallery in the Fine Arts <strong>Building</strong>. This collaborative dialogue event, co-sponsored by …",
    "uuid": "3a81ac01-6afa-4150-9887-030ce50c4656"
  },
  {
    "title": "WWU Caregivers &amp;amp; Parents (CAP) group and Community Connect to Host &amp;#039;Childcare Challenges in Whatcom County&amp;#039; Nov. 15",
    "body": "<p>The WWU Caregivers &amp;amp; Parents (CAP) group and Community Connect will host &quot;Childcare Challenges in Whatcom County&quot;&amp;nbsp;from 2:30-4 p.m. on Friday,&amp;nbsp;Nov.15&amp;nbsp;at the Fairhaven Auditorium.  The event is free and open to the public, but registration is required; please register here&amp;nbsp;on the WWU HR registration site. Presented by the Opportunity Council, Brigid Collins, Bellingham Healthy Motherhood, and the Bellingham YMCA.",
    "excerpt": "… program that invites local nonprofits to lead dialogues and <strong>build</strong> collaborative relationships with the …",
    "uuid": "1b7f25e3-36d1-4604-9cfd-918ba095365a"
  },
  {
    "title": "Community Connect: Economic Vitality &amp;amp; Sustainability in Whatcom County on Oct. 17",
    "body": "<p>The Port of Bellingham and The Downtown Bellingham Partnership will lead a dialogue about the scope of work, current growth initiatives and programs, and how each organization works to support the economic development in Bellingham and Whatcom County. What does sustainable economic development look like in Whatcom County? Join the discussion on Thursday, Oct. 17, from noon to 1 p.m. in Haggard Hall 222.  Reserve your seat on the WWU HR registration site.",
    "excerpt": "… program that invites local nonprofits to lead dialogues and <strong>build</strong> collaborative relationships with the …",
    "uuid": "04ee5e17-8889-480e-8cdd-33358c1e4cf9"
  },
  {
    "title": "Wise &amp;amp; Well U announces new workshops",
    "body": "<p>Wise &amp;amp; Well U is a program dedicated to faculty and staff well-being by promoting financial, personal, and physical wellness. The program offers a variety of workshops, activities, and tools to support overall well-being. We partner with other campus members, state agencies, and local organizations to cover a variety of topics. Through Wise &amp;amp; Well U, the Community Connect program invites local nonprofits to lead dialogues and build collaborative relationships with the University community. We also help promote the state’s voluntary wellness program, SmartHealth.",
    "excerpt": "… program invites local nonprofits to lead dialogues and <strong>build</strong> collaborative relationships with the University …",
    "uuid": "d8ac9b7d-2390-4281-9fc4-65cd86034f90"
  },
  {
    "title": "Western Launches New ‘Go Northwest of Ordinary’ Out-of-State Recruitment Campaign",
    "body": "<p>Western Washington University has just launched “Go Northwest of Ordinary,” a targeted recruitment campaign focused on attracting new out-of-state students to come to Western next fall.  Locations in which Western has historically done well in recruiting, including Northern California, Colorado, Oregon, Idaho, Texas and the Chicago metro area will be the initial focus areas of the campaign which will run through May.",
    "excerpt": "… advertising and IP targeting, as well as an awareness-<strong>building</strong> campaign inside Denver International Airport during …",
    "uuid": "ccc39762-a2f3-4782-9b2c-9c65db812bfa"
  }
];

const testSearchData3 = [
  {
    "title": "Design Department to host November speaker series starting Nov. 6",
    "body": "<p>The Western Washington University Department of Design presents Michael Ellsworth, Peter Stocker, and Sheryl Cababa for a series of lectures in November. Ellsworth is a Principal/Co-founder of Civilization; Stocker is a Principal of the firm MG2; Cababa is Executive Creative Director at Artefact. All lectures are at 6 p.m. in Miller Hall 138. The talks are free and open to the public.  On Wednesday, Nov. 6, Ellsworth will discuss how design has the power to inspire conversations, create community and promote collective action.",
    "excerpt": "… of Civilization. Civilization is a design practice that <strong>builds</strong> identity systems, digital experiences, printed …",
    "uuid": "7e26712c-1011-4953-bb2d-079c5fd57d52"
  },
  {
    "title": "WWU&amp;#039;s Deb Donovan and her students fight to save the Salish Sea&amp;#039;s pinto abalone",
    "body": "<p>The pinto abalone once ranged in large numbers from Alaska to Baja California, but overfishing, poaching and other factors have caused them to become functionally extinct in the Salish Sea; according to figures from the Washington Dept. of Fish &amp;amp; Wildlife, the species has experienced a 98 percent drop in numbers since 1998 in local waters alone.",
    "excerpt": "… an area and clear space by grazing down micro-algae that <strong>builds</strong> up on the seafloor. By constantly grazing, abalone …",
    "uuid": "501008e9-5c22-469e-8231-979f34b027e9"
  }
];

const testSearchCount = [
  {
    "counter": "1"
  },
  {
    "counter": "2"
  },
  {
    "counter": "3"
  },
  {
    "counter": "4"
  },
  {
    "counter": "5"
  },
  {
    "counter": "6"
  },
  {
    "counter": "7"
  },
  {
    "counter": "8"
  },
  {
    "counter": "9"
  },
  {
    "counter": "10"
  },
  {
    "counter": "11"
  },
  {
    "counter": "12"
  },
  {
    "counter": "13"
  },
  {
    "counter": "14"
  },
  {
    "counter": "15"
  },
  {
    "counter": "16"
  },
  {
    "counter": "17"
  },
  {
    "counter": "18"
  },
  {
    "counter": "19"
  },
  {
    "counter": "20"
  }
];

export async function searchProd(endpoint, query) {
  const params = new URLSearchParams({'query': query.q, 'page': query.page});
  const response = await fetch(endpoint + '?' + params.toString());
  const result = await response.json();

  return result;
}

export async function searchTest(endpoint, query) {
  const { q, page } = query;

  return new Promise((resolve, reject) => {
    switch (Number(page)) {

      case 2:
        resolve({
          data: testSearchData3,
          count: testSearchCount.length
        });
        break;

      case 1:
        resolve({
          data: testSearchData2,
          count: testSearchCount.length
        });
        break;

      default:
        resolve({
          data: testSearchData1,
          count: testSearchCount.length
        });

    }
  });
}
