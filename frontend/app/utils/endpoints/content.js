require('es6-promise').polyfill();
require('isomorphic-fetch');

/**
 * This test data is example output from the Drupal JSON:API core module. It is
 * standardized based on the JSON:API specification.
 *
 * See https://jsonapi.org/ and
 * https://www.drupal.org/docs/8/modules/jsonapi/api-overview for more
 * information.
 */
const rawTestItem = {
  "jsonapi": {
    "version": "1.0",
    "meta": {
      "links": {
        "self": {
          "href": "http://jsonapi.org/format/1.0/"
        }
      }
    }
  },
  "data": [
    {
      "type": "node--content",
      "id": "358be32c-3e33-4ecc-9a2f-fcfef8b78d8b",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b?resourceVersion=id%3A1"
        }
      },
      "attributes": {
        "drupal_internal__nid": 1,
        "drupal_internal__vid": 1,
        "langcode": "en",
        "revision_timestamp": "2020-05-19T22:11:09+00:00",
        "revision_log": null,
        "status": true,
        "title": "Test Article 1",
        "created": "2020-05-19T22:09:59+00:00",
        "changed": "2020-05-19T23:22:17+00:00",
        "promote": true,
        "sticky": false,
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        },
        "field_body": {
          "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n",
          "format": "basic_html",
          "processed": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        "field_created": "2020-05-01T00:00:00+00:00",
        "field_images": [],
        "field_updated": "2020-05-20T00:00:00+00:00",
        "field_url": "https://westerntoday.wwu.edu/"
      },
      "relationships": {
        "node_type": {
          "data": {
            "type": "node_type--node_type",
            "id": "506f3005-d67e-4db2-984b-b5701e7c2182"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/node_type?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/node_type?resourceVersion=id%3A1"
            }
          }
        },
        "revision_uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/revision_uid?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/revision_uid?resourceVersion=id%3A1"
            }
          }
        },
        "uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/uid?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/uid?resourceVersion=id%3A1"
            }
          }
        },
        "field_tags": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "ccc6a22e-9765-4a89-95fe-8162146691a2"
            },
            {
              "type": "taxonomy_term--tags",
              "id": "19534381-1b3f-497d-aa27-ea92776fa961"
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/field_tags?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/field_tags?resourceVersion=id%3A1"
            }
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "taxonomy_term--tags",
      "id": "ccc6a22e-9765-4a89-95fe-8162146691a2",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2"
        }
      },
      "attributes": {
        "drupal_internal__tid": 1,
        "drupal_internal__revision_id": 1,
        "langcode": "en",
        "revision_created": "2020-05-19T22:11:09+00:00",
        "revision_log_message": null,
        "status": true,
        "name": "Tag 1",
        "description": null,
        "weight": 0,
        "changed": "2020-05-19T22:11:09+00:00",
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        }
      },
      "relationships": {
        "vid": {
          "data": {
            "type": "taxonomy_vocabulary--taxonomy_vocabulary",
            "id": "61d569ac-7b27-471b-9e5a-8b95e776ef48"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/vid"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/relationships/vid"
            }
          }
        },
        "revision_user": {
          "data": null,
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/revision_user"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/relationships/revision_user"
            }
          }
        },
        "parent": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "virtual",
              "meta": {
                "links": {
                  "help": {
                    "href": "https://www.drupal.org/docs/8/modules/json-api/core-concepts#virtual",
                    "meta": {
                      "about": "Usage and meaning of the 'virtual' resource identifier."
                    }
                  }
                }
              }
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/parent"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/relationships/parent"
            }
          }
        }
      }
    },
    {
      "type": "taxonomy_term--tags",
      "id": "19534381-1b3f-497d-aa27-ea92776fa961",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961"
        }
      },
      "attributes": {
        "drupal_internal__tid": 2,
        "drupal_internal__revision_id": 2,
        "langcode": "en",
        "revision_created": "2020-05-19T23:22:17+00:00",
        "revision_log_message": null,
        "status": true,
        "name": "Tag 2",
        "description": null,
        "weight": 0,
        "changed": "2020-05-19T23:22:17+00:00",
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        }
      },
      "relationships": {
        "vid": {
          "data": {
            "type": "taxonomy_vocabulary--taxonomy_vocabulary",
            "id": "61d569ac-7b27-471b-9e5a-8b95e776ef48"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/vid"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/relationships/vid"
            }
          }
        },
        "revision_user": {
          "data": null,
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/revision_user"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/relationships/revision_user"
            }
          }
        },
        "parent": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "virtual",
              "meta": {
                "links": {
                  "help": {
                    "href": "https://www.drupal.org/docs/8/modules/json-api/core-concepts#virtual",
                    "meta": {
                      "about": "Usage and meaning of the 'virtual' resource identifier."
                    }
                  }
                }
              }
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/parent"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/relationships/parent"
            }
          }
        }
      }
    }
  ],
  "meta": {
    "count": "1"
  },
  "links": {
    "self": {
      "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b"
    }
  }
};

/**
 * This test data is example output from the Drupal JSON:API core module. It is
 * standardized based on the JSON:API specification.
 *
 * See https://jsonapi.org/ and
 * https://www.drupal.org/docs/8/modules/jsonapi/api-overview for more
 * information.
 */
const rawTestData = {
  "jsonapi": {
    "version": "1.0",
    "meta": {
      "links": {
        "self": {
          "href": "http://jsonapi.org/format/1.0/"
        }
      }
    }
  },
  "data": [
    {
      "type": "node--content",
      "id": "358be32c-3e33-4ecc-9a2f-fcfef8b78d8b",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b?resourceVersion=id%3A1"
        }
      },
      "attributes": {
        "drupal_internal__nid": 1,
        "drupal_internal__vid": 1,
        "langcode": "en",
        "revision_timestamp": "2020-05-19T22:11:09+00:00",
        "revision_log": null,
        "status": true,
        "title": "Test Article 1",
        "created": "2020-05-19T22:09:59+00:00",
        "changed": "2020-05-19T23:22:17+00:00",
        "promote": true,
        "sticky": false,
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        },
        "field_body": {
          "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n",
          "format": "basic_html",
          "processed": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        "field_created": "2020-05-01T00:00:00+00:00",
        "field_images": [],
        "field_updated": "2020-05-20T00:00:00+00:00",
        "field_url": "https://westerntoday.wwu.edu/"
      },
      "relationships": {
        "node_type": {
          "data": {
            "type": "node_type--node_type",
            "id": "506f3005-d67e-4db2-984b-b5701e7c2182"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/node_type?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/node_type?resourceVersion=id%3A1"
            }
          }
        },
        "revision_uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/revision_uid?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/revision_uid?resourceVersion=id%3A1"
            }
          }
        },
        "uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/uid?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/uid?resourceVersion=id%3A1"
            }
          }
        },
        "field_tags": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "ccc6a22e-9765-4a89-95fe-8162146691a2"
            },
            {
              "type": "taxonomy_term--tags",
              "id": "19534381-1b3f-497d-aa27-ea92776fa961"
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/field_tags?resourceVersion=id%3A1"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/358be32c-3e33-4ecc-9a2f-fcfef8b78d8b/relationships/field_tags?resourceVersion=id%3A1"
            }
          }
        }
      }
    },
    {
      "type": "node--content",
      "id": "9cbded2c-e0c6-4184-8d48-fbdc1944c656",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656?resourceVersion=id%3A2"
        }
      },
      "attributes": {
        "drupal_internal__nid": 2,
        "drupal_internal__vid": 2,
        "langcode": "en",
        "revision_timestamp": "2020-05-19T23:22:53+00:00",
        "revision_log": null,
        "status": true,
        "title": "Test Article 2",
        "created": "2020-05-19T23:22:19+00:00",
        "changed": "2020-05-19T23:22:56+00:00",
        "promote": true,
        "sticky": false,
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        },
        "field_body": {
          "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n",
          "format": "basic_html",
          "processed": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        "field_created": "2020-05-01T00:00:00+00:00",
        "field_images": [],
        "field_updated": "2020-05-19T00:00:00+00:00",
        "field_url": "https://westerntoday.wwu.edu/"
      },
      "relationships": {
        "node_type": {
          "data": {
            "type": "node_type--node_type",
            "id": "506f3005-d67e-4db2-984b-b5701e7c2182"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/node_type?resourceVersion=id%3A2"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/relationships/node_type?resourceVersion=id%3A2"
            }
          }
        },
        "revision_uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/revision_uid?resourceVersion=id%3A2"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/relationships/revision_uid?resourceVersion=id%3A2"
            }
          }
        },
        "uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/uid?resourceVersion=id%3A2"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/relationships/uid?resourceVersion=id%3A2"
            }
          }
        },
        "field_tags": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "19534381-1b3f-497d-aa27-ea92776fa961"
            },
            {
              "type": "taxonomy_term--tags",
              "id": "b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9"
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/field_tags?resourceVersion=id%3A2"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/9cbded2c-e0c6-4184-8d48-fbdc1944c656/relationships/field_tags?resourceVersion=id%3A2"
            }
          }
        }
      }
    },
    {
      "type": "node--content",
      "id": "be4a0425-100a-4dcd-aba7-5f9efdc965e8",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8?resourceVersion=id%3A3"
        }
      },
      "attributes": {
        "drupal_internal__nid": 3,
        "drupal_internal__vid": 3,
        "langcode": "en",
        "revision_timestamp": "2020-05-19T23:57:18+00:00",
        "revision_log": null,
        "status": true,
        "title": "Test Article 3",
        "created": "2020-05-19T23:56:53+00:00",
        "changed": "2020-05-19T23:57:20+00:00",
        "promote": true,
        "sticky": false,
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        },
        "field_body": {
          "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n",
          "format": "basic_html",
          "processed": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        "field_created": "2020-05-19T00:00:00+00:00",
        "field_images": [],
        "field_updated": "2020-05-01T00:00:00+00:00",
        "field_url": "https://westerntoday.wwu.edu/"
      },
      "relationships": {
        "node_type": {
          "data": {
            "type": "node_type--node_type",
            "id": "506f3005-d67e-4db2-984b-b5701e7c2182"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/node_type?resourceVersion=id%3A3"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/relationships/node_type?resourceVersion=id%3A3"
            }
          }
        },
        "revision_uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/revision_uid?resourceVersion=id%3A3"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/relationships/revision_uid?resourceVersion=id%3A3"
            }
          }
        },
        "uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/uid?resourceVersion=id%3A3"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/relationships/uid?resourceVersion=id%3A3"
            }
          }
        },
        "field_tags": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9"
            },
            {
              "type": "taxonomy_term--tags",
              "id": "8622ed85-bca8-4195-bc1f-f3871af19500"
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/field_tags?resourceVersion=id%3A3"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/be4a0425-100a-4dcd-aba7-5f9efdc965e8/relationships/field_tags?resourceVersion=id%3A3"
            }
          }
        }
      }
    },
    {
      "type": "node--content",
      "id": "95af7dff-0096-4fc6-beee-db25969a633a",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a?resourceVersion=id%3A4"
        }
      },
      "attributes": {
        "drupal_internal__nid": 4,
        "drupal_internal__vid": 4,
        "langcode": "en",
        "revision_timestamp": "2020-05-19T23:57:49+00:00",
        "revision_log": null,
        "status": true,
        "title": "Test Article 4",
        "created": "2020-05-19T23:57:25+00:00",
        "changed": "2020-05-19T23:57:52+00:00",
        "promote": true,
        "sticky": false,
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        },
        "field_body": {
          "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n",
          "format": "basic_html",
          "processed": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        "field_created": "2020-05-01T00:00:00+00:00",
        "field_images": [],
        "field_updated": "2020-05-19T00:00:00+00:00",
        "field_url": "https://westerntoday.wwu.edu/"
      },
      "relationships": {
        "node_type": {
          "data": {
            "type": "node_type--node_type",
            "id": "506f3005-d67e-4db2-984b-b5701e7c2182"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/node_type?resourceVersion=id%3A4"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/relationships/node_type?resourceVersion=id%3A4"
            }
          }
        },
        "revision_uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/revision_uid?resourceVersion=id%3A4"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/relationships/revision_uid?resourceVersion=id%3A4"
            }
          }
        },
        "uid": {
          "data": {
            "type": "user--user",
            "id": "4b2ce9aa-45aa-402e-97e6-af2af1cb0d36"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/uid?resourceVersion=id%3A4"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/relationships/uid?resourceVersion=id%3A4"
            }
          }
        },
        "field_tags": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "8622ed85-bca8-4195-bc1f-f3871af19500"
            },
            {
              "type": "taxonomy_term--tags",
              "id": "c80aa995-797c-42b1-ab28-3257b90fd8f2"
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/field_tags?resourceVersion=id%3A4"
            },
            "self": {
              "href": "http://webtech.local:8080/api/node/content/95af7dff-0096-4fc6-beee-db25969a633a/relationships/field_tags?resourceVersion=id%3A4"
            }
          }
        }
      }
    }
  ],
  "included": [
    {
      "type": "taxonomy_term--tags",
      "id": "ccc6a22e-9765-4a89-95fe-8162146691a2",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2"
        }
      },
      "attributes": {
        "drupal_internal__tid": 1,
        "drupal_internal__revision_id": 1,
        "langcode": "en",
        "revision_created": "2020-05-19T22:11:09+00:00",
        "revision_log_message": null,
        "status": true,
        "name": "Tag 1",
        "description": null,
        "weight": 0,
        "changed": "2020-05-19T22:11:09+00:00",
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        }
      },
      "relationships": {
        "vid": {
          "data": {
            "type": "taxonomy_vocabulary--taxonomy_vocabulary",
            "id": "61d569ac-7b27-471b-9e5a-8b95e776ef48"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/vid"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/relationships/vid"
            }
          }
        },
        "revision_user": {
          "data": null,
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/revision_user"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/relationships/revision_user"
            }
          }
        },
        "parent": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "virtual",
              "meta": {
                "links": {
                  "help": {
                    "href": "https://www.drupal.org/docs/8/modules/json-api/core-concepts#virtual",
                    "meta": {
                      "about": "Usage and meaning of the 'virtual' resource identifier."
                    }
                  }
                }
              }
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/parent"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/ccc6a22e-9765-4a89-95fe-8162146691a2/relationships/parent"
            }
          }
        }
      }
    },
    {
      "type": "taxonomy_term--tags",
      "id": "19534381-1b3f-497d-aa27-ea92776fa961",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961"
        }
      },
      "attributes": {
        "drupal_internal__tid": 2,
        "drupal_internal__revision_id": 2,
        "langcode": "en",
        "revision_created": "2020-05-19T23:22:17+00:00",
        "revision_log_message": null,
        "status": true,
        "name": "Tag 2",
        "description": null,
        "weight": 0,
        "changed": "2020-05-19T23:22:17+00:00",
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        }
      },
      "relationships": {
        "vid": {
          "data": {
            "type": "taxonomy_vocabulary--taxonomy_vocabulary",
            "id": "61d569ac-7b27-471b-9e5a-8b95e776ef48"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/vid"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/relationships/vid"
            }
          }
        },
        "revision_user": {
          "data": null,
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/revision_user"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/relationships/revision_user"
            }
          }
        },
        "parent": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "virtual",
              "meta": {
                "links": {
                  "help": {
                    "href": "https://www.drupal.org/docs/8/modules/json-api/core-concepts#virtual",
                    "meta": {
                      "about": "Usage and meaning of the 'virtual' resource identifier."
                    }
                  }
                }
              }
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/parent"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/19534381-1b3f-497d-aa27-ea92776fa961/relationships/parent"
            }
          }
        }
      }
    },
    {
      "type": "taxonomy_term--tags",
      "id": "b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/taxonomy_term/tags/b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9"
        }
      },
      "attributes": {
        "drupal_internal__tid": 3,
        "drupal_internal__revision_id": 3,
        "langcode": "en",
        "revision_created": "2020-05-19T23:22:56+00:00",
        "revision_log_message": null,
        "status": true,
        "name": "Tag 3",
        "description": null,
        "weight": 0,
        "changed": "2020-05-19T23:22:56+00:00",
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        }
      },
      "relationships": {
        "vid": {
          "data": {
            "type": "taxonomy_vocabulary--taxonomy_vocabulary",
            "id": "61d569ac-7b27-471b-9e5a-8b95e776ef48"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9/vid"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9/relationships/vid"
            }
          }
        },
        "revision_user": {
          "data": null,
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9/revision_user"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9/relationships/revision_user"
            }
          }
        },
        "parent": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "virtual",
              "meta": {
                "links": {
                  "help": {
                    "href": "https://www.drupal.org/docs/8/modules/json-api/core-concepts#virtual",
                    "meta": {
                      "about": "Usage and meaning of the 'virtual' resource identifier."
                    }
                  }
                }
              }
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9/parent"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/b427a0b1-4b12-42c4-aa8c-8e8a74fc37a9/relationships/parent"
            }
          }
        }
      }
    },
    {
      "type": "taxonomy_term--tags",
      "id": "8622ed85-bca8-4195-bc1f-f3871af19500",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/taxonomy_term/tags/8622ed85-bca8-4195-bc1f-f3871af19500"
        }
      },
      "attributes": {
        "drupal_internal__tid": 4,
        "drupal_internal__revision_id": 4,
        "langcode": "en",
        "revision_created": "2020-05-19T23:57:20+00:00",
        "revision_log_message": null,
        "status": true,
        "name": "Tag 4",
        "description": null,
        "weight": 0,
        "changed": "2020-05-19T23:57:20+00:00",
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        }
      },
      "relationships": {
        "vid": {
          "data": {
            "type": "taxonomy_vocabulary--taxonomy_vocabulary",
            "id": "61d569ac-7b27-471b-9e5a-8b95e776ef48"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/8622ed85-bca8-4195-bc1f-f3871af19500/vid"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/8622ed85-bca8-4195-bc1f-f3871af19500/relationships/vid"
            }
          }
        },
        "revision_user": {
          "data": null,
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/8622ed85-bca8-4195-bc1f-f3871af19500/revision_user"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/8622ed85-bca8-4195-bc1f-f3871af19500/relationships/revision_user"
            }
          }
        },
        "parent": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "virtual",
              "meta": {
                "links": {
                  "help": {
                    "href": "https://www.drupal.org/docs/8/modules/json-api/core-concepts#virtual",
                    "meta": {
                      "about": "Usage and meaning of the 'virtual' resource identifier."
                    }
                  }
                }
              }
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/8622ed85-bca8-4195-bc1f-f3871af19500/parent"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/8622ed85-bca8-4195-bc1f-f3871af19500/relationships/parent"
            }
          }
        }
      }
    },
    {
      "type": "taxonomy_term--tags",
      "id": "c80aa995-797c-42b1-ab28-3257b90fd8f2",
      "links": {
        "self": {
          "href": "http://webtech.local:8080/api/taxonomy_term/tags/c80aa995-797c-42b1-ab28-3257b90fd8f2"
        }
      },
      "attributes": {
        "drupal_internal__tid": 5,
        "drupal_internal__revision_id": 5,
        "langcode": "en",
        "revision_created": "2020-05-19T23:57:52+00:00",
        "revision_log_message": null,
        "status": true,
        "name": "Tag 5",
        "description": null,
        "weight": 0,
        "changed": "2020-05-19T23:57:52+00:00",
        "default_langcode": true,
        "revision_translation_affected": true,
        "path": {
          "alias": null,
          "pid": null,
          "langcode": "en"
        }
      },
      "relationships": {
        "vid": {
          "data": {
            "type": "taxonomy_vocabulary--taxonomy_vocabulary",
            "id": "61d569ac-7b27-471b-9e5a-8b95e776ef48"
          },
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/c80aa995-797c-42b1-ab28-3257b90fd8f2/vid"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/c80aa995-797c-42b1-ab28-3257b90fd8f2/relationships/vid"
            }
          }
        },
        "revision_user": {
          "data": null,
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/c80aa995-797c-42b1-ab28-3257b90fd8f2/revision_user"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/c80aa995-797c-42b1-ab28-3257b90fd8f2/relationships/revision_user"
            }
          }
        },
        "parent": {
          "data": [
            {
              "type": "taxonomy_term--tags",
              "id": "virtual",
              "meta": {
                "links": {
                  "help": {
                    "href": "https://www.drupal.org/docs/8/modules/json-api/core-concepts#virtual",
                    "meta": {
                      "about": "Usage and meaning of the 'virtual' resource identifier."
                    }
                  }
                }
              }
            }
          ],
          "links": {
            "related": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/c80aa995-797c-42b1-ab28-3257b90fd8f2/parent"
            },
            "self": {
              "href": "http://webtech.local:8080/api/taxonomy_term/tags/c80aa995-797c-42b1-ab28-3257b90fd8f2/relationships/parent"
            }
          }
        }
      }
    }
  ],
  "meta": {
    "count": "4"
  },
  "links": {
    "self": {
      "href": "http://webtech.local:8080/api/node/content?include=field_tags"
    }
  }
};

/**
 * Build an object of included entities keyed to their unique ID in Drupal.
 *
 * As in the example data above, the response from the JSON:API will have a
 * "data" property, as well as an "included" operty if the right parameter is
 * provided. This "included" array must be transformed in order for the result
 * to be consumed easily by downstream components.
 *
 * For example, given the following "included" array:
 *
 *    [
 *      0: {
 *        id: 12345,
 *        attributes: {
 *          name: 'Foo'
 *        }
 *      },
 *      1: {
 *        id: 67890,
 *        attributes: {
 *          name: 'Bar'
 *          }
 *        }
 *      }
 *    ]
 *
 * Would become:
 *
 *    {
 *      12345: { <defined by transform function> }
 *      67890: {               ...               }
 *    }
 *
 * The parameter "transform" is a callback function that should return the
 * desired representation of each entity.
 */
function mapIncludedToEntityId(included, transform) {
  return included.reduce((obj, include) => {
    const { id } = include;

    obj[id] = transform(include);

    return obj;
  }, {});
}

/**
 * Extract entity data on an entity reference.
 *
 * Accepts the raw field data (e.g. relationships.field_name) and the map
 * returend from mapIncludedToEntityId().
 */
function parseRelationship(field, map) {
  const { data } = field;

  return data.map(reference => map[reference.id]);
}

/**
 * Map the taxonomy terms (tags) from the "included" property to their Drupal
 * field name in the "data" property.
 *
 * Tags are parsed from the "included" property, extracting the entity ID of
 * the term and the term name. "included" is enabled by passing the parameter
 * "include=field_tags", and can include other entity reference relationships as
 * well via the same convention.
 *
 * This is then inserted into the data property for further downstream processing.
 */
function insertTags({ data, included }) {
  const map = mapIncludedToEntityId(included, tag => ({
    id: tag.id,
    name: tag.attributes.name
  }));

  return data.map(item => {
    item.attributes.field_tags = parseRelationship(item.relationships.field_tags, map);
    return item;
  });
}

/**
 * Extract the fields we want and map them to the desired properties of the
 * final item object.
 *
 * Returns a parsed list of articles according to the format:
 *
 *  {
 *    title:        String  // The item title.
 *    excerpt:   String  // The truncated body text of the content.
 *    link:         String  // URL to the content.
 *    images:       Array   // List of image links.
 *    tags:         Array   // An array of objects containing tag label and id.
 *    date:         String  // Date.
 *    dateModified: String  // Modified date.
 *  }
 */
function transformData({ id, attributes }) {
  return {
    uuid: id,
    title: attributes.title,
    excerpt: attributes.field_body.value.match(/^(?:[^\s]+\s+\n?){1,30}/),
    body: attributes.field_body.value,
    link: attributes.field_url,
    created: attributes.field_created,
    modified: attributes.field_updated,
    tags: attributes.field_tags
  };
}

/**
 * Fetch all items from the given endpoint.
 *
 * The endpoint must adhere to the JSON:API specification.
 */
export async function fetchItems(endpoint) {
  const response = await fetch(endpoint, {
    method: 'GET',
    mode: 'cors'
  });

  const result = await response.json();
  const data = insertTags(result);

  return data.map(item => transformData(item));
}

/**
 * Return the test data from the top of this file.
 */
export async function fetchTestItems(endpoint) {
  return new Promise((resolve, reject) => {
    const data = insertTags(rawTestData);

    resolve(data.map(item => transformData(item)));
  });
}

/**
 * Fetch a single item.
 */
export async function fetchItem(endpoint) {
  const response = await fetch(endpoint, {
    method: 'GET',
    mode: 'cors'
  });

  const result = await response.json();
  const data = transformData(result.data);

  return data;
}

/**
 * Return the test data for a single item.
 */
export async function fetchTestItem(endpoint) {
  return new Promise((resolve, reject) => {
    const data = insertTags(rawTestItem);

    resolve(data.map(item => transformData(item)).pop());
  });
}
