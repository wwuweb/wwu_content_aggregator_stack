import { fetchTestItems, fetchItems, fetchTestItem, fetchItem } from '../endpoints/content';
import { searchTest, searchProd } from '../endpoints/search';

/**
 * This module defines the endpoint functions used for fetching data from the
 * backend (Drupal).
 *
 * These function definitions should then be passed to the Backend context
 * provider, defined in components/Backend. This allows any component in the
 * application to query an endpoint, without introducing coupling at the
 * component level.
 *
 * Using a context makes it easy to unit test individual components by simply
 * passing a mock endpoint to the Backend context provider during the test.
 * Technically, this file is unnecessary for making components testable, but it
 * allows us to provide automatic demo output based on the value of the
 * environment variable INCOMPLETE_BACKEND.
 */

export const search = process.env.INCOMPLETE_BACKEND ? searchTest : searchProd;

export const content = process.env.INCOMPLETE_BACKEND ? fetchTestItem : fetchItem;
