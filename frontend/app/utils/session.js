export const options = {
  cookie: {
    httpOnly: true,
    sameSite: 'Strict'
  }
};
