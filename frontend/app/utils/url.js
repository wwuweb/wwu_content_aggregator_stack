export function params(components) {
  const builder = new URLSearchParams(components);
  return '?' + builder.toString();
}
