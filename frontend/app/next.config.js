/**
 * Defines environment variables and other configuration for the app.
 *
 * BASE_URL: The publicly-accessible hostname of the frontend application. This
 * will need to be changed depending on whether the app is running locally or in
 * production.
 *
 * BACKEND_URL: The hostname at which the backend can be reached. Since the app
 * is running in Docker, this is defined in the configuration for the backend
 * container and generally shouldn't need to be changed.
 *
 * INCOMPLETE_BACKEND: Intended to be set to true if the app is running
 * standalone without the backend. Should be set to false in production or if
 * running the backend container. Otherwise, demo data will be inserted and
 * backend APIs will not be queried.
 *
 * See https://nextjs.org/docs/api-reference/next.config.js/introduction for
 * additional uses of this file.
 * 
 * NOTE FROM DONAGHA BRANCH:
 * previous values are as following...
 * BASE_URL: 'http://webtech.local:3000',
 * BACKEND_URL: 'http://backend',
 * INCOMPLETE_BACKEND: true,
 */
module.exports = {
  env : {
    INCOMPLETE_BACKEND: false,
    BASE_URL: 'http://webtech.local:3000',
    BACKEND_URL: 'http://backend',
    CAS_URL: 'https://websso.wwu.edu:443/cas',
    SEARCH_PATH: '/search',
    LOGIN_PATH: '/login',
    LOGOUT_PATH: '/logout',
    CAS_PATH: '/cas'
  }
};
