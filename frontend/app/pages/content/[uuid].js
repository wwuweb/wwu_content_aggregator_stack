import { useState, useEffect } from 'react';
import { withRouter } from 'next/router';
import { AddTag, AddComment, CommentList, Content, ContentLayout, Main, PageTitle, TagList, useBackend, withAuth, withAuthServerSideProps } from '../../components';
import styles from '../../styles/modules/ContentPage.module.css';

const ContentPage = (props) => {
  const { query } = props.router;
  const [ loading, setLoading ] = useState(true);
  const [ uuid, setUuid ] = useState(query.uuid);
  const [ item, setItem ] = useState([]);
  const { content } = useBackend();

  const refresh = () => {
    setLoading(true);
    content(uuid).then(result => {
      setLoading(false);
      setItem(result);
    });
  }

  useEffect(() => {
    refresh();
  }, [uuid]);

  if (loading) {
    return <Main>Loading...</Main>
  }
  else {
    return (
      <Main>
        <ContentLayout>
          <PageTitle>{item.title}</PageTitle>
          <Content value={item} />
          <div className={styles.contentPageSection}>
            <TagList tags={item.tags} contentId={item.uuid} refreshContent={refresh} />
          </div>
          <div className={styles.contentPageSection}>
            <AddTag contentId={item.uuid} currentTags={item.tags} refreshContent={refresh} />
          </div>
          <div className={styles.contentPageSection}>
            <CommentList contentId={item.uuid} item={item.comments} refreshContent={refresh} />
          </div>
          <div className={styles.contentPageSection}>
            <AddComment comments={item.comments} refreshContent={refresh} />
          </div>
        </ContentLayout>
      </Main>
    );
  }
}

export const getServerSideProps = withAuthServerSideProps();

export default withAuth(withRouter(ContentPage));
