import { Container } from '../components'
import { Backend } from '../components'
import { content, search } from '../utils/endpoints'

import '../styles/reset.css'
import '../styles/Layout.css'
import '../styles/Typography.css'
import '../styles/Elements.css'

const MyApp = ({ Component, pageProps }) => (
  <Backend endpoints={{
    search: (query) => search(process.env.BASE_URL + '/api/search', query),
    content: (uuid) => content(process.env.BASE_URL + `/api/content?id=${uuid}`)
  }}>
    <Container>
      <Component {...pageProps} />
    </Container>
  </Backend>
);

export default MyApp;
