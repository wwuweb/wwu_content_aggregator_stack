import axios from 'axios';

const endpoint = process.env.BACKEND_URL + '/api/node/content';

export default async (req, res) => {
  const {
    headers,
    method,
    url,
  } = req;

  const { searchParams } = new URL(url, process.env.BASE_URL);
  let uuid = searchParams.get("id");
  let queryURL = endpoint + `/${uuid}` 

  switch (method) {
    case 'GET':
      try {
        const { data } = await axios.get(queryURL, { timeout: 1000 });
        res.status(200).json(data);
      }
      catch (error) {
        const debug = { errors: [ error ] };

        if (error.response) {
          res.status(error.response.status).json(debug);
        }
        else {
          res.status(500).json(debug);
        }
      }
      break;
    default:
      res.setHeader('Allow', ['GET']);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}
