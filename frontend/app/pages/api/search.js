import axios from 'axios';

const searchEndpoint = process.env.BACKEND_URL + '/rest/export/search';
const countEndpoint = process.env.BACKEND_URL + '/rest/export/count';

async function fetchData(search) {
  const { data } = await axios.get(searchEndpoint + search, { timeout: 1000 });
  return data;
}

async function fetchCount(search) {
  const { data } = await axios.get(countEndpoint + search, { timeout: 1000 });
  return data;
}

export default async (req, res) => {
  const { method, url, } = req;
  const { search } = new URL(url, process.env.BASE_URL);

  switch (method) {
    case 'GET':
      try {
        const data = await fetchData(search);
        const count = await fetchCount(search);

        res.status(200).json({
          'data': data,
          'count': count.length
        });
      }
      catch (error) {
        const debug = { errors: [ error ] };

        console.log('An error occurred.');
        console.log(error);

        if (error.response) {
          res.status(error.response.status).json(debug);
        }
        else {
          res.status(500).json(debug);
        }
      }
      break;
    default:
      res.setHeader('Allow', ['GET']);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}
