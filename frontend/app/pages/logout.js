import { useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { applySession } from 'next-session';
import { Main } from '../components';
import { options } from '../utils/session';

export async function getServerSideProps({req, res}) {
  await applySession(req, res, options);

  req.session.authorized = false;
  req.session.destroy();

  return {
    props: {
      authorized: false
    }
  }
}

const LogoutPage = (props) => {
  const router = useRouter();

  useEffect(() => {
    router.replace(process.env.LOGIN_PATH);
  });

  return (
    <Main>
      <Head>
        <title>Redirecting...</title>
      </Head>
    </Main>
  );
}

export default LogoutPage;
