import { useRouter } from 'next/router';
import { Main, PageTitle, Search, withAuth, withAuthServerSideProps } from '../components';

const SearchPage = () => {
  const { query } = useRouter();

  return (
    <Main>
      <PageTitle>Search</PageTitle>
      <Search path={process.env.SEARCH_PATH} text={query.q} landing={false} />
    </Main>
  );
}

export const getServerSideProps = withAuthServerSideProps();

export default withAuth(SearchPage);
