import { useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { applySession } from 'next-session';
import { Main, Login, PageTitle } from '../components';
import { options } from '../utils/session';

export async function getServerSideProps({req, res}) {
  const service = process.env.BASE_URL + '/cas';
  const request = process.env.CAS_URL + '/login' + '?service=' + encodeURI(service);

  await applySession(req, res, options);

  return {
    props: {
      authorized: req.session.authorized || false,
      request: request
    }
  };
}

const LoginPage = (props) => {
  const { authorized } = props;
  const { request } = props;
  const router = useRouter();

  useEffect(() => {
    if (authorized) {
      router.replace('/');
    }
  });

  if (authorized) {
    return (
      <Main>
        <Head>
          <title>Redirecting...</title>
        </Head>
      </Main>
    );
  }
  else {
    return (
      <Main>
        <PageTitle>Log In</PageTitle>
        <Login link={request} />
      </Main>
    );
  }
}

export default LoginPage;
