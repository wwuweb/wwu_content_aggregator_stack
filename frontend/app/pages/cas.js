import { useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { applySession } from 'next-session';
import { Main } from '../components';
import { validate } from '../utils/cas';
import { options } from '../utils/session';

export async function getServerSideProps({ req, res, query }) {
  const { ticket } = query;
  const service = process.env.BASE_URL + process.env.CAS_PATH;

  await applySession(req, res, options);

  if (ticket) {
    req.session.authorized = await validate(ticket, service);
  }
  else {
    req.session.authorized = false;
  }

  return {
    props: {
      authorized: req.session.authorized
    }
  }
}

const CasService = (props) => {
  const { authorized } = props;
  const router = useRouter();

  useEffect(() => {
    if (authorized) {
      router.replace('/');
    }
    else {
      router.replace(process.env.LOGIN_PATH);
    }
  });

  return (
    <Main>
      <Head>
        <title>Redirecting...</title>
      </Head>
    </Main>
  );
}

export default CasService;
