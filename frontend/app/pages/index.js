import { Main, PageTitle, Search, withAuth, withAuthServerSideProps } from '../components';

const Index = () => (
  <Main>
    <PageTitle>Search</PageTitle>
    <Search path={process.env.SEARCH_PATH} landing={true} />
  </Main>
);

export const getServerSideProps = withAuthServerSideProps();

export default withAuth(Index);
