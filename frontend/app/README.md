# Webtech Content Aggregator

Developer documentation for the frontend application. Please refer to the [React Docs](https://reactjs.org/docs/getting-started.html) and the [Next.js Docs](https://nextjs.org/docs/getting-started) for additional reference on the patterns used in this app.

## Technologies

 - React
 - Next.js

## Structure

 - **/pages:** A feature of next.js, Pages define routes accessible from the UI. They can make use of some additional API functions from next.js, such as getServerSideProps, which inject props before sending and rendering in the user's browser.
 - **/pages/api:** Also a next.js feature, these provide server-side endpoints accessible in the browser at `/api`.  We use these to proxy the Drupal JSON API endpoints so that they are not accessible clientside via the external network.
 - **/components:** The catch-all for React components. Components are exported in `/components/index.js`.
 - **/styles:** Stylesheets are located here, and can be included globally in `/pages/_app.js`.
 - **/styles/modules:** Module-specific stylesheets are included here. These files must follow the `.module.css` naming convention, and be included in the respective component for which they are named.
 - **/utils:** Various helper functions and domain-specific logic is placed here. This includes client-side code for making API calls, for easier testing and abstraction.

## Local Development

This app will run locally on port 3000, external to Docker, if a Node.js environment is installed. First, run `npm install`, then `npm run build` and finally `npm run dev`. This will spin up the Next.js development server, which will allow you to edit the application in real time and see detailed error output directly in the browser. The configuration key `incompleteBackend` is provided to inject some basic mocks of the backend interface, so that placeholder data is available for local development without a live backend.

Note that there is no separate local configuration, so the configuration documented below will need to be modified to allow the app to function at a URL other than production.

## Configuration

Configuration is located in `next.config.js`. The sub-keys of the `env` key are injected as environment variables at runtime, accessible via `process.env`, the standard for Node.js. Additional environment variables can be injected via the `docker-compose.yml` file as desired.

## Design

This application is developed with the latest versions of React and Next.js. This means using React functional components and React Hooks. Some effort was also made to implement atomic UI components as stateless, lifting state to parent components where possible.

React Context is used in `/pages/_app.js` to inject the backend API functions and endpoint URLs to lower-level components. This allows these functions to be decoupled from components for easier testing.

Authentication is performed via a Higher-Order Component (essentially a decorator), found in `/components/withAuth.js`. This component must be used to wrap any page that should be protected by authentication.
